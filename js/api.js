alertify.set('notifier','position', 'top-center');
$( document ).ready(function() {
    submitBerium();

});

function submitBerium(){
    $(document).on('submit', '#submitKasae', function(event){
        event.preventDefault();
        $('#submit').attr('disabled', true);
        $('#submit').text('Please wait..');
        var data;
        data = new FormData(this);
        data.append("email_to", 'suleiman1984@gmail.com');
        $.ajax({
            url:'http://167.71.137.248/emailapi/public/api/postEnquiry',
            method:"POST",
            data: data,
            contentType:false,
            processData:false,
            success:function(data) {
                if(data.status) {
                    alertify.success(data.message);
                    $('#submit').attr('disabled', false);
                    $('#submit').text('Send Message');
                    $(':input', '#submitKasae')
                        .not(':button,:submit,:hidden')
                        .val('')
                        .prop('checked', false)
                        .prop('selected', false);
                }else{
                    alertify.error(data.message);
                    $('#submit').attr('disabled', false);
                    $('#submit').text('Send Message');
                }
            },
            error:function(data) {
                alertify.error('Internal server error! or invalid data');
                $('#submit').attr('disabled', false);
                $('#submit').text('Send Message');
            }

        });
    });
}

function newslatter() {
    var email=document.getElementById('newsemail').value;
    if(email===''){
        alertify.error('Enter email');
    }else {
        alertify.success('You have successfully subscribed to monthly newsletter');
    }
}


